$(function(){

    $('.prettycheckbox a').click(function(){
        var me = $(this);
        if(me.hasClass('checked')){
            me.parent().find('input[type="checkbox"]').attr('checked', 'checked')
        } else {
            me.parent().find('input[type="checkbox"]').removeAttr('checked');
        }
    });

	var dropbox = $('#dropbox'),
		message = $('.message', dropbox),
        fileFieldName = $('input[name="fileFieldName"]').val(),
        globalFile = '',
		maxNumberOfFiles = dropbox.attr('data-max-files'),
		countFiles = 0;


	if(typeof form_config !== 'undefined') {
		var url = form_config.env_url + '/admin/upload-file';


		dropbox.filedrop({
			// The name of the $_FILES entry:
			paramname: 'file',
			data: {key: form_config.objectKey, id: form_config.objectId},
			maxfiles: maxNumberOfFiles,
			maxfilesize: 40,
			url: url,

			uploadFinished: function (i, file, response) {
				$('.files').append('<b><a href="/uploads/'+form_config.objectKey+'/'+form_config.objectId+'/'+response.fileName+'" target="_blank" data-lightbox="gallery">' + response.fileName + '</b><input type="hidden" value="' + response.fileName + '" name="' + fileFieldName + '"/><br/>');
				$.data(file).addClass('done');
			},

			error: function (err, file) {
				switch (err) {
					case 'BrowserNotSupported':
						var message = Translator.trans('files.html5_not_supported');
						alert(message);
						break;
					case 'TooManyFiles':
						var message = Translator.transChoice('files.max', maxNumberOfFiles, {"count" : maxNumberOfFiles});
						alert(message);
						break;
					case 'FileTooLarge':
						var message = Translator.transChoice('files.too_big', 1, {'file_name' : file.name, file_size: '40MB' });
						alert(message);
						break;
					default:
						alert(message);
						break;
				}
			},

			// Called before each upload is started
			beforeEach: function (file) {
				if(countFiles < maxNumberOfFiles) {
					countFiles++;
					globalFile = file;
				} else {
					var message = Translator.transChoice('files.max', maxNumberOfFiles, {"count" : maxNumberOfFiles});
					alert(message);
					return false;
				}

				if(!form_config.objectId) {
					var message = Translator.trans('files.first_save');
					alert(message);
					return false;
				}
			},

			uploadStarted: function (i, file, len) {
				createImage(file);
			},

			progressUpdated: function (i, file, progress) {
				$.data(file).find('.progress').width(progress);
			}

		});

	}
	
	var template = '<div class="preview">'+
						'<span class="imageHolder">'+
							'<img />'+
							'<span class="uploaded"></span>'+
						'</span>'+
						'<div class="progressHolder">'+
							'<div class="progress"></div>'+
						'</div>'+
					'</div>'; 
	
	
	function createImage(file){

		var preview = $(template), 
			image = $('img', preview);
			
		var reader = new FileReader();
		
		image.width = 100;
		image.height = 100;
		
		reader.onload = function(e){

			// e.target.result holds the DataURL which
			// can be used as a source of the image:
            if(globalFile.type.match(/^image\//)) {
                image.attr('src', e.target.result);
            } else 	if(globalFile.type.match(/pdf/)){
                image.attr('src','/bundles/resymfcms/img/pdf-icon.png');
            } else {
                image.attr('src','/bundles/resymfcms/img/file.png');
            }
		};
		
		// Reading the file as a DataURL. When finished,
		// this will trigger the onload function above:
		reader.readAsDataURL(file);
		
		message.hide();
		preview.appendTo(dropbox);
		
		// Associating a preview container
		// with the file, using jQuery's $.data():
		
		$.data(file,preview);
	}

	function showMessage(msg){
		message.html(msg);
	}

});