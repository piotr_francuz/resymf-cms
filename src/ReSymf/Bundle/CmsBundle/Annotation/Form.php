<?php
/**
 * Created by PhpStorm.
 * User: ppf
 * Date: 24.11.13
 * Time: 01:07
 */

namespace ReSymf\Bundle\CmsBundle\Annotation;

/**
 * Class Form
 * @package ReSymf\Bundle\CmsBundle\Annotation
 *
 * @Annotation
 *
 * @author Piotr Francuz <piotr.francuz@bizneslan.pl>
 */
class Form
{
    private $type = 'text'; // bool, relation, editor, file, number, text, email, color, tel, url
    private $required = true;
    private $length;
    private $display = true; // true, false
    private $readOnly = false; // in set to true field is redonly
    private $editLabel = 'Edit Object';
    private $menuLabel = 'Obejcts';
    private $createLabel = 'Create Object';
    private $showLabel = 'Show Object';
    private $withoutLink = false; // true, false
    private $fieldLabel;  // some string displaying in field label
    private $dateFormat ="DD-MM-YYYY hh:mm:ss";
    private $relationType;  //oneToMany, manyToMany, oneToOne
    private $autoInput = false; //currentDateTime, currentUserId, uniqueSlug
    private $class; // e.g. ReSymf\Bundle\ProjectManagerBundle\Entity\Sprint
    private $displayField; // name of field from relation class displayed in multiselect or singleselect relation field
    private $multiSelectInEditForm = false; // if you want ti show multiselect in edit form
    private $unique = false; //unique values in field
    private $targetEntityField; // key in admin.yml that point to relation class, eg. in sprint set to project
    private $role; // role field, used only in user object
    private $customWidth = 12;
    private $maxFiles = 5;

    /**
     * save all @Form annotations from Entity to this Form object
     *
     * @param $options
     * @throws \InvalidArgumentException
     */
    public function __construct($options)
    {
        foreach ($options as $key => $value) {
            if (!property_exists($this, $key)) {
                throw new \InvalidArgumentException(sprintf('Property "%s" does not exist', $key));
            }
            $this->$key = $value;
        }
    }

    /**
     * @return int
     */
    public function getMaxFiles() {
        return $this->maxFiles;
    }

    /**
     * @param int $maxFiles
     */
    public function setMaxFiles( $maxFiles ) {
        $this->maxFiles = $maxFiles;
    }



    /**
     * @return mixed
     */
    public function getCustomWidth()
    {
        return $this->customWidth;
    }

    /**
     * @param mixed $customWidth
     */
    public function setCustomWidth($customWidth)
    {
        $this->customWidth = $customWidth;
    }

    /**
     * @return boolean
     */
    public function getUnique()
    {
        return $this->unique;
    }

    /**
     * @param boolean $unique
     */
    public function setUnique($unique)
    {
        $this->unique = $unique;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return mixed
     */
    public function getTargetEntityField()
    {
        return $this->targetEntityField;
    }

    /**
     * @param mixed $targetEntityField
     */
    public function setTargetEntityField($targetEntityField)
    {
        $this->targetEntityField = $targetEntityField;
    }

    /**
     * @return string
     */
    public function getShowLabel()
    {
        return $this->showLabel;
    }

    /**
     * @param string $showLabel
     */
    public function setShowLabel($showLabel)
    {
        $this->showLabel = $showLabel;
    }

    /**
     * @return string
     */
    public function getMenuLabel()
    {
        return $this->menuLabel;
    }

    /**
     * @param string $menuLabel
     */
    public function setMenuLabel($menuLabel)
    {
        $this->menuLabel = $menuLabel;
    }

    /**
     * @return string
     */
    public function getDateFormat()
    {
        return $this->dateFormat;
    }

    /**
     * @param string $dateFormat
     */
    public function setDateFormat($dateFormat)
    {
        $this->dateFormat = $dateFormat;
    }

    /**
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param mixed $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }
 
    /**
     * @return mixed
     */
    public function getWithoutLink()
    {
        return $this->withoutLink;
    }

    /**
     * @param mixed $class
     */
    public function setWithoutLink($class)
    {
        $this->withoutLink = $class;
    }

    // can set value as:
    // 1. 'currentTime' - set current time as value when created
    // 2. 'currentUser' - set current user id as value when created

    /**
     * @return mixed
     */
    public function getRelationType()
    {
        return $this->relationType;
    }

    /**
     * @param mixed $relationType
     */
    public function setRelationType($relationType)
    {
        $this->relationType = $relationType;
    }

    /**
     * @return mixed
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * @return mixed
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @return mixed
     */
    public function getReadOnly()
    {
        return $this->readOnly;
    }

    /**
     * @return mixed
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getEditLabel()
    {
        return $this->editLabel;
    }

    /**
     * @return mixed
     */
    public function getCreateLabel()
    {
        return $this->createLabel;
    }

    /**
     * @return mixed
     */
    public function getFieldLabel()
    {
        return $this->fieldLabel;
    }

    /**
     * @return boolean
     */
    public function getAutoInput()
    {
        return $this->autoInput;
    }

    /**
     * @return mixed
     */
    public function getDisplayField()
    {
        return $this->displayField;
    }

    /**
     * @param mixed $displayField
     */
    public function setDisplayField($displayField)
    {
        $this->displayField = $displayField;
    }

    /**
     * @return boolean
     */
    public function getMultiSelectInEditForm()
    {
        return $this->multiSelectInEditForm;
    }

    /**
     * @param boolean $multiSelectInEditForm
     */
    public function setMultiSelectInEditForm($multiSelectInEditForm)
    {
        $this->multiSelectInEditForm = $multiSelectInEditForm;
    }

} 
