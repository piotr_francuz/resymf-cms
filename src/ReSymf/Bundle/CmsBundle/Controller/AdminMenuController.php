<?php

namespace ReSymf\Bundle\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AdminMenuController
 * @package ReSymf\Bundle\CmsBundle\Controller
 *
 * @author Piotr Francuz <piotr.francuz@bizneslan.pl>
 */
class AdminMenuController extends Controller
{

    private $sortKey;

    /**
     * Profile show/edit url
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profileAction()
    {
        $request = $this->container->get('request');
        $routeName = $request->get('_route');
        $em = $this->getDoctrine()->getManager();


        $objectType = 'ReSymf\Bundle\CmsBundle\Entity\User';

        $user = $this->get('security.context')->getToken()->getUser();

        $adminConfigurator = $this->get('resymfcms.configurator.admin');

        $objectConfigurator = $this->get('resymfcms.object.configurator');

        $annotationReader = $this->get('resymfcms.annotation.reader');

        $formConfig = $annotationReader->readFormAnnotation($objectType);
        $formConfig->objectKey = 'user';

        // this service save values from request to editobject
        $objectSaver = $this->get('resymfcms.object.saver');

        if ($request->isMethod('POST') && $user) {

            // save values from request to object
            $objectSaver->saveObject($formConfig, $user, $request, 'user');

            // check unique values in object
            $objectConfigurator->checkUniqueValuesFromAnnotations($user, 'user');

            $em->persist($user);
            $em->flush();

        }

        $multiSelectValues = $objectConfigurator->generateMultiSelectOptions($objectType, $user);

        return $this->render(
            'ReSymfCmsBundle:adminmenu:create.html.twig',
            array(
                'menu' => $adminConfigurator->getAdminConfig(),
                'site_config' => $adminConfigurator->getSiteConfig(),
                'form_config' => $formConfig,
                'route' => $routeName,
                'edit_object' => $user,
                'multi_select' => $multiSelectValues,
                'object_type' => 'profile'
            )
        );
    }

    /**
     * Display dashboard information
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dashboardAction()
    {
        $adminConfigurator = $this->get('resymfcms.configurator.admin');

        $request = $this->container->get('request');
        $routeName = $request->get('_route');

        return $this->render('ReSymfCmsBundle:adminmenu:dashboard.html.twig', array('menu' => $adminConfigurator->getAdminConfig(), 'site_config' => $adminConfigurator->getSiteConfig(), 'object_type' => 'dashboard', 'route' => $routeName));
    }

    /**
     * Lists all entities.
     *
     * @param $type
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction($type)
    {
        if (!$type) {
            return $this->redirect($this->generateUrl('resymf_admin_dashboard'), 301);
        }

        $request = $this->container->get('request');
        $routeName = $request->get('_route');

        $adminConfigurator = $this->get('resymfcms.configurator.admin');
        $objectMapper = $this->get('resymfcms.object.mapper');

        $objectType = $objectMapper->getMappedObject($type);
        $annotationReader = $this->get('resymfcms.annotation.reader');

        $tableConfig = $annotationReader->readTableAnnotation($objectType);

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository($objectType)
            ->createQueryBuilder('q')
            ->getQuery()
            ->getResult();

        return $this->render(
            'ReSymfCmsBundle:adminmenu:list.html.twig',
            array(
                'menu' => $adminConfigurator->getAdminConfig(),
                'site_config' => $adminConfigurator->getSiteConfig(),
                'route' => $routeName,
                'entities' => $entities,
                'table_config' => $tableConfig,
                'object_type' => $type
            )
        );
    }


    /**
     * Creates a new object base on url and request parameters.
     *
     * @param $type
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction($type)
    {
        if (!$type) {
            return $this->redirect($this->generateUrl('resymf_admin_dashboard'), 301);
        }
        $em = $this->getDoctrine()->getManager();

        $request = $this->container->get('request');
        $routeName = $request->get('_route');

        $adminConfigurator = $this->get('resymfcms.configurator.admin');
        $objectConfigurator = $this->get('resymfcms.object.configurator');

        $objectMapper = $this->get('resymfcms.object.mapper');

        $objectType = $objectMapper->getMappedObject($type);
        $annotationReader = $this->get('resymfcms.annotation.reader');

        $formConfig = $annotationReader->readFormAnnotation($objectType);

        $env = 'prod';
        $env_url = '/';
        if ($this->container->getParameter('kernel.environment') == 'dev') {
            $env = 'dev';
            $env_url = '/app_dev.php';
        }
        $formConfig->env = $env;
        $formConfig->env_url = $env_url;

        // this service save values from request to editobject
        $objectSaver = $this->get('resymfcms.object.saver');


        if ($request->isMethod('POST')) {
            $object = new $objectType();

            // save values from request to object
            $objectSaver->saveObject($formConfig, $object, $request, $type);

            // check unique values in object
            $objectConfigurator->checkUniqueValuesFromAnnotations($object, $type);

            // set autoinput values in object
            $objectConfigurator->setAutoInputValuesFromAnnotations($objectType, $object, $type, $request);

            $em->persist($object);
            $em->flush();
            return $this->redirect($this->generateUrl('object_edit', array('type' => $type, 'id' => $object->getId())), 301);
        }

        if (!isset($object)) {
            $object = false;
        }


        $multiSelectValues = $objectConfigurator->generateMultiSelectOptions($objectType, $object);

        return $this->render(
            'ReSymfCmsBundle:adminmenu:create.html.twig',
            array(
                'menu' => $adminConfigurator->getAdminConfig(),
                'site_config' => $adminConfigurator->getSiteConfig(),
                'form_config' => $formConfig,
                'route' => $routeName,
                'multi_select' => $multiSelectValues,
                'object_type' => $type
            )
        );
    }


    /**
     * @param $type
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function editAction($type, $id)
    {
        if (!$id) {
            return $this->redirect($this->generateUrl('resymf_admin_dashboard'), 301);
        }
        $request = $this->container->get('request');
        $routeName = $request->get('_route');

        $adminConfigurator = $this->get('resymfcms.configurator.admin');
        $objectMapper = $this->get('resymfcms.object.mapper');

        $objectConfigurator = $this->get('resymfcms.object.configurator');

        $objectType = $objectMapper->getMappedObject($type);
        $annotationReader = $this->get('resymfcms.annotation.reader');

        $em = $this->getDoctrine()->getManager();

        $editObject = $em->getRepository($objectType)
            ->createQueryBuilder('q')
            ->where('q.id = :id')
            ->setParameter('id', $id)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$editObject) {
            throw new \Exception('Object not found');
        }

        $formConfig = $annotationReader->readFormAnnotation($objectType);
        $formConfig->objectKey = $type;
        $formConfig->objectId = $editObject->getId();

        $env = 'prod';
        $env_url = '/';
        if ($this->container->getParameter('kernel.environment') == 'dev') {
            $env = 'dev';
            $env_url = '/app_dev.php';
        }
        $formConfig->env = $env;
        $formConfig->env_url = $env_url;

        // this service save values from request to editobject
        $objectSaver = $this->get('resymfcms.object.saver');

        $objectHistory = $this->get('resymfcms.object.history');

        $historyCount = $objectHistory->getHistoryCount($editObject, $type);

        if ($request->isMethod('POST') && $editObject) {

            // save object version in history, get count of saved object
            $historyCount = $objectHistory->saveToHistory($editObject, $type);

            // save values from request to object
            $editObject = $objectSaver->saveObject($formConfig, $editObject, $request, $type);

            // check unique values in object
            $objectConfigurator->checkUniqueValuesFromAnnotations($editObject, $type);

            $em->persist($editObject);
            $em->flush();
        }

        $multiSelectValues = $objectConfigurator->generateMultiSelectOptions($objectType, $editObject);

        return $this->render(
            'ReSymfCmsBundle:adminmenu:create.html.twig',
            array(
                'menu' => $adminConfigurator->getAdminConfig(),
                'site_config' => $adminConfigurator->getSiteConfig(),
                'form_config' => $formConfig,
                'route' => $routeName,
                'edit_object' => $editObject,
                'multi_select' => $multiSelectValues,
                'object_type' => $type,
                'history_count' => $historyCount
            )
        );
    }

    /**
     * @param $type
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function showAction($type, $id)
    {
        if (!$id) {
            return $this->redirect($this->generateUrl('resymf_admin_dashboard'), 301);
        }
        $request = $this->container->get('request');
        $routeName = $request->get('_route');

        $adminConfigurator = $this->get('resymfcms.configurator.admin');
        $objectMapper = $this->get('resymfcms.object.mapper');

        $objectConfigurator = $this->get('resymfcms.object.configurator');

        $objectType = $objectMapper->getMappedObject($type);
        $annotationReader = $this->get('resymfcms.annotation.reader');

        $formConfig = $annotationReader->readFormAnnotation($objectType);
        $formConfig->objectType = $objectType;

        $em = $this->getDoctrine()->getManager();

        $editObject = $em->getRepository($objectType)
            ->createQueryBuilder('q')
            ->where('q.id = :id')
            ->setParameter('id', $id)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$editObject) {
            // TODO: może redirect do tworzenia ?
            throw new \Exception('Object not found');
        }

        $formConfig->objectKey = $type;
        $formConfig->objectId = $editObject->getId();

        $env = 'prod';
        $env_url = '';
        if ($this->container->getParameter('kernel.environment') == 'dev') {
            $env = 'dev';
            $env_url = '/app_dev.php';
        }
        $formConfig->env = $env;
        $formConfig->env_url = $env_url;

        $objectHistory = $this->get('resymfcms.object.history');

        // save object version in history, get count of saved object
        $historyCount = $objectHistory->getHistoryCount($editObject, $type);

        $multiSelectValues = $objectConfigurator->generateMultiSelectOptions($objectType, $editObject);
        
	return $this->render(
            'ReSymfCmsBundle:adminmenu:show.html.twig',
            array(
                'menu' => $adminConfigurator->getAdminConfig(),
                'site_config' => $adminConfigurator->getSiteConfig(),
                'form_config' => $formConfig,
                'route' => $routeName,
                'edit_object' => $editObject,
                'multi_select' => $multiSelectValues,
                'object_type' => $type,
                'history_count' => $historyCount
            )
        );
    }

    public function deleteAction($type, $id)
    {
        $request = $this->container->get('request');
        $url = $request->headers->get('referer');

        if (!$id) {
            return $this->redirect($this->generateUrl('resymf_admin_dashboard'), 301);
        }

        $objectMapper = $this->get('resymfcms.object.mapper');
        $objectType = $objectMapper->getMappedObject($type);
        $em = $this->getDoctrine()->getManager();

        // TODO: if no object display error
        $query = $em->getRepository($objectType)->createQueryBuilder('q')->where('q.id IN(:id)')->setParameter('id', explode(',', $id))->getQuery();
        $editObjects = $query->getResult();

        if (!isset($editObjects[0])) {
            return $this->redirect($this->generateUrl('object_list', array('type' => $type)), 301);
        }

        foreach ($editObjects as $editObject) {
            $em->remove($editObject);
            $em->flush();
        }

        //return $this->redirect($this->generateUrl('object_list', array('type' => $type)), 301);
        return $this->redirect($url, 301);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function settingsAction()
    {
        $request = $this->container->get('request');
        $routeName = $request->get('_route');
        $em = $this->getDoctrine()->getManager();

        $objectType = 'ReSymf\Bundle\CmsBundle\Entity\Settings';
        $type = 'settings';

        // ALways first object
        $editObject = $em->getRepository($objectType)
            ->createQueryBuilder('q')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$editObject) {
            $editObject = new $objectType();
        }

        $objectConfigurator = $this->get('resymfcms.object.configurator');
        $adminConfigurator = $this->get('resymfcms.configurator.admin');

        $annotationReader = $this->get('resymfcms.annotation.reader');

        $formConfig = $annotationReader->readFormAnnotation($objectType);

        // this service save values from request to editobject
        $objectSaver = $this->get('resymfcms.object.saver');


        if ($request->isMethod('POST') && $editObject) {

            // save values from request to object
            $editObject = $objectSaver->saveObject($formConfig, $editObject, $request, $type);

            // check unique values in object
            $objectConfigurator->checkUniqueValuesFromAnnotations($editObject, $type);

            $em = $this->getDoctrine()->getManager();
            $em->persist($editObject);
            $em->flush();
        }


        $multiSelectValues = $objectConfigurator->generateMultiSelectOptions($objectType, $editObject);

        return $this->render(
            'ReSymfCmsBundle:adminmenu:create.html.twig',
            array(
                'menu' => $adminConfigurator->getAdminConfig(),
                'site_config' => $adminConfigurator->getSiteConfig(),
                'form_config' => $formConfig,
                'route' => $routeName,
                'edit_object' => $editObject,
                'multi_select' => $multiSelectValues,
                'object_type' => $type
            )
        );
    }

    /**
     * display custom page in admin menu
     *
     * @param $slug
     * @return Response
     */
    public function customAdminPageAction($slug)
    {
        $request = $this->container->get('request');

        $adminConfigurator = $this->get('resymfcms.configurator.admin');

        $item = $adminConfigurator->checkItemIfExistInMenu($slug);
        if (!$item) {
            return $this->render('ReSymfCmsBundle:cms:notfound.html.twig');
        }

        if (isset($item['template'])) {
            $template = $item['template'];
        } else {
            return $this->render('ReSymfCmsBundle:cms:notfound.html.twig');
        }

        if (isset($item['label'])) {
            $label = $item['label'];
        } else {
            $label = 'Custom Page';
        }

        $routeName = $request->get('_route');

        $menu = $adminConfigurator->getAdminConfig($slug);

        // get objects used in custom page
        $objects = array();
        if (isset($menu[$slug]['class']) && $menu[$slug]['class']) {
            $class = $menu[$slug]['class'];
            $em = $this->getDoctrine()->getManager();
            $objects = $em->getRepository($class)
                ->createQueryBuilder('q')
                ->getQuery()
                ->getResult();

            // auto sorting by sort_key set in admin.yml, from lowest to highest
            if (isset($menu[$slug]['sort_key'])) {
                $this->sortKey = $menu[$slug]['sort_key'];
                usort($objects, array($this, 'cmp'));
            }
        }

        $formConfig = new \stdClass();
        $env = 'prod';
        $env_url = '';
        if ($this->container->getParameter('kernel.environment') == 'dev') {
            $env = 'dev';
            $env_url = '/app_dev.php';
        }
        $formConfig->env = $env;
        $formConfig->env_url = $env_url;

        return $this->render(
            $template,
            array(
                'label' => $label,
                'site_config' => $adminConfigurator->getSiteConfig(),
                'menu' => $adminConfigurator->getAdminConfig($slug),
                'slug' => $slug,
                'objects' => $objects,
                'route' => $routeName,
                'form_config' => $formConfig,
                'object_type' => 'custom'
            )
        );
    }

    /**
     * function used in usort, sort from lowest to highest
     *
     * @param $a
     * @param $b
     * @return int
     */
    public function cmp($a, $b)
    {
        $sortKey = $this->sortKey;
        $getter = 'get' . ucfirst($sortKey);

        if (method_exists($a, $getter)) {
            if ($a->$getter() == $b->$getter()) {
                return 0;
            }
            return ($a->$getter() < $b->$getter()) ? -1 : 1;
        } else {
            return 0;
        }
    }
}
