<?php
/**
 * Created by PhpStorm.
 * User: Piotr
 * Date: 2014-05-24
 * Time: 10:29
 */

namespace ReSymf\Bundle\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class FileManager
 * @package ReSymf\Bundle\CmsBundle\Controller
 *
 * @author Piotr Francuz <piotr.francuz@bizneslan.pl
 */
class FileManagerController extends Controller {

	public function getUploadDir() {
		return $this->get( 'kernel' )->getRootDir() . '/../web/uploads/';
	}

	public function uploadAction() {

		$uploadDir = $this->getUploadDir();
		$request   = $this->container->get( 'request' );

		$file      = $request->files->get( 'file' );
		if(!$file) {
			throw new \Exception('File not found');
		}

		$name      = $file->getClientOriginalName();


		$type      = $request->get( 'key' );
		$id        = $request->get( 'id' );

		if( $type && $id ) {
			$uploadDir = $uploadDir . $type;

			$fs = new Filesystem();

			if (!$fs->exists($uploadDir)) {
				$fs->mkdir($uploadDir);
			}

			$uploadDir = $uploadDir . '/' . $id . '/';
			if (!$fs->exists($uploadDir)) {
				$fs->mkdir($uploadDir);
			}
		}

		$helper = $this->get('resymfcms.helper');
		$name = $helper->slugify( $name );
		$name = str_replace('jpg', '.jpg', $name);
		$name = str_replace('jpeg', '.jpeg', $name);
		$name = str_replace('png', '.png', $name);

		$newName = $name;
		while ( $fs->exists( $uploadDir . $newName ) ) {
			$newName = rand( 0, 99 ) . '-' . $name;
		}


		$response = new JsonResponse();

		if ( move_uploaded_file( $file->getPathName(), $uploadDir . $newName ) ) {
			$response->setData( array( 'success' => true, 'status' => 'File was uploaded successfuly!', 'fileName' => $name ) );
		} else {
			$response->setData( array( 'success' => false, 'status' => 'Something went wrong with your upload!' ) );
		}

		return $response;
	}

	public function removeAction() {

		$uploadDir = $this->getUploadDir();
		$request   = $this->container->get( 'request' );

		$file      = $request->files->get( 'file' );
		if(!$file) {
			throw new \Exception('File not found');
		}

		$name      = $file->getClientOriginalName();
		$type      = $request->get( 'key' );
		$id        = $request->get( 'id' );
		$uploadDir = $uploadDir . $type;
		$uploadDir = $uploadDir . '/' . $id . '/';

		$fs = new Filesystem();
		$fs->remove($uploadDir . $name);

		$response = new JsonResponse();
		$response->setData(array( 'status' => 'File was removed successfuly!', 'fileName' => $name ));

		return $response;
	}

	public function fileAction() {

		$helper = $this->get('resymfcms.helper');

		$uploadDir = $this->getUploadDir();
		$request   = $this->container->get( 'request' );

		$type      = $helper->clean($request->get( 'key' ));
		$id        = $helper->clean($request->get( 'id' ));
		$name      = $this->checkFileName($helper->clean($request->get( 'name' )));

		if($id && $type) {
			// check permissions for $id and $type (get by id and type and check access to object files)
			$uploadDir = $uploadDir . $type . '/' . $id . '/';
		}

		$finder = new Finder();
		$finder->name($name);

		foreach ($finder->in($uploadDir) as $file) {

			$contents = $file->getContents();

			$headers = array(
				'Content-Type' => mime_content_type($uploadDir . $name),
				'Content-Disposition' => 'attachment; filename="'.$name.'"'
			);

			return new Response($contents, 200, $headers);

			break;
		}

		$response = new JsonResponse();
		$response->setData(array( 'status' => 'false'));
		return $response;
	}

	protected function checkFileName($name) {
		$nameArray = explode('/', $name);
		return array_pop($nameArray);
	}

	public function getExtension( $file_name ) {
		$ext = explode( '.', $file_name );
		$ext = array_pop( $ext );

		return strtolower( $ext );
	}
} 
