<?php
/**
 * Created by PhpStorm.
 * User: ppf
 * Date: 16.03.14
 * Time: 08:33
 */

namespace ReSymf\Bundle\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class CmsRoutingController
 * @package ReSymf\Bundle\CmsBundle\Controller
 *
 * @author Piotr Francuz <piotr.francuz@bizneslan.pl>
 *
 */
class CmsRoutingController extends Controller {

	public function indexAction( $slug ) {

		$adminConfigurator = $this->get( 'resymfcms.configurator.admin' );
		$site_config       = $adminConfigurator->getSiteConfig();
		$em                = $this->getDoctrine()->getManager();

		if ( empty( $slug ) && ! empty( $site_config['main_page'] ) ) {

			$pageObject = $em->getRepository( 'ReSymf\Bundle\CmsBundle\Entity\Page' )->createQueryBuilder( 'p' )
			                 ->select( 'p' )
			                 ->where( 'p.id = :id' )
			                 ->setParameter( 'id', $site_config['main_page'] )
			                 ->getQuery()
			                 ->getOneOrNullResult();
		} else {

			$pageObject = $em->getRepository( 'ReSymf\Bundle\CmsBundle\Entity\Page' )->createQueryBuilder( 'p' )
			                 ->select( 'p' )
			                 ->where( 'p.slug = :slug' )
			                 ->setParameter( 'slug', $slug )
			                 ->getQuery()
			                 ->getOneOrNullResult();
		}

		$menusWithPages = array();
		$menus          = $em->getRepository( 'ReSymf\Bundle\CmsBundle\Entity\Menu' )
		                     ->createQueryBuilder( 'p' )
		                     ->getQuery()
		                     ->getResult();

		foreach ( $menus as $menu ) {
			$tempMenu          = array();
			$tempMenu['title'] = $menu->getName();

			$pages         = $menu->getPages();
			$tempMenuPages = array();
			foreach ( $pages as $page ) {
				$tempPage          = array();
				$tempPage['title'] = $page->getTitle();
				$tempPage['url']   = $page->getSlug();
				$tempPage['order'] = $page->getMenuOrder();
				$tempMenuPages[]   = $tempPage;
			}

			usort( $tempMenuPages, array( $this, 'cmp' ) );

			$tempMenu['pages'] = $tempMenuPages;

			$menusWithPages[] = $tempMenu;
		}

		if ( ! $pageObject ) {
			return $this->objectPageAction( $slug, $menusWithPages );
		}

		$template = 'index.html.twig';
		if ( $pageObject->getTemplate() ) {
			$template = $pageObject->getTemplate();
		}

		$frontendConfigurator = $this->get( 'resymfcms.frontend.configurator' );

		// get page parameter from request
		$request    = $this->container->get( 'request' );
		$pageNumber = $request->get( 'page' );
		$objects    = $frontendConfigurator->getObjectsForView( $template, $pageNumber );

		$env     = 'prod';
		$env_url = '';
		if ( $this->container->getParameter( 'kernel.environment' ) == 'dev' ) {
			$env     = 'dev';
			$env_url = '/app_dev.php';
		}
		$site_config['env']     = $env;
		$site_config['env_url'] = $env_url;

		return $this->render( $site_config['templates_bundle'] . ':cms:' . $template,
			array(
				'pageObject'   => $pageObject,
				'site_config'  => $site_config,
				'menus'        => $menusWithPages,
				'csrf_token' => $this->container->get('form.csrf_provider')->generateCsrfToken('authenticate'),
				'objects'      => $objects['elements'],
				'count'        => $objects['count'],
				'current_page' => $objects['page'],
				'all_pages'    => $objects['all_pages'],
				'table_config' => $objects['table_config']
			)
		);
	}

	public function objectPageAction( $slug, $menusWithPages ) {

		$frontendConfigurator = $this->get( 'resymfcms.frontend.configurator' );
		$returnObject         = $frontendConfigurator->getObjectForSlug( $slug );

		if ( ! is_array( $returnObject ) && ! $returnObject['object'] ) {
			return $this->notFoundAction();
		}

		$adminConfigurator = $this->get( 'resymfcms.configurator.admin' );
		$site_config       = $adminConfigurator->getSiteConfig();


		return $this->render( $site_config['templates_bundle'] . ':cms:' . $returnObject['template'],
			array(
				'pageObject'  => new \stdClass(),
				'site_config' => $site_config,
				'csrf_token' => $this->container->get('form.csrf_provider')->generateCsrfToken('authenticate'),
				'menus'       => $menusWithPages,
				'objects'     => array( $returnObject['object'] )
			)
		);
	}

	public function notFoundAction() {
		return $this->render( 'ReSymfCmsBundle:cms:notfound.html.twig' );
	}

	public function accessDeniedAction() {
		return $this->render( 'ReSymfCmsBundle:cms:accessdenied.html.twig' );
	}

	public function cmp( $a, $b ) {
		if ( $a['order'] == $b['order'] ) {
			return 0;
		}

		return ( $a['order'] < $b['order'] ) ? - 1 : 1;
	}

} 
