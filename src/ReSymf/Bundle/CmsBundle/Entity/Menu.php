<?php
/**
 * Created by PhpStorm.
 * User: Piotr
 * Date: 2014-12-23
 * Time: 05:52
 */

namespace ReSymf\Bundle\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Query\Expr\Base;
use ReSymf\Bundle\CmsBundle\Annotation\Table;
use ReSymf\Bundle\CmsBundle\Annotation\Form;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Class Menu
 * @package ReSymf\Bundle\CmsBundle\Entity
 *
 * @ORM\Table()
 * @ORM\Entity
 *
 * @Table(sorting=true, paging=true, pageSize=10, filtering=true)
 * @Form(menuLabel="Menu", editLabel="Edycja menu", createLabel="Dodaj menu", showLabel="Menu")
 *
 * @author Piotr Francuz <piotr.francuz@bizneslan.pl>
 */
class Menu {

    /**
     * @var integer
     *
     * @Form(display=false)
     * @Table(display=false)
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Table(hideOnDevice="tablet,phone", label="Nazwa")
     * @Form(fieldLabel="Nazwa",type="text",required=true)
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @var Terms
     *
     * @Table(display=false)
     * @Form(type="relation", relationType="multiselect", class="ReSymf\Bundle\CmsBundle\Entity\Page", fieldLabel="Strony", targetEntityField="page", displayField="title")
     *
     * @ORM\ManyToMany(targetEntity="Page")
     */
    private $pages;

    function __construct()
    {
        $this->pages = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Terms
     */
    public function getPages()
    {
        return $this->pages;
    }

    public function setPages($pages)
    {
        $this->pages = $pages;
    }
}