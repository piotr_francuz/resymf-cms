<?php

namespace ReSymf\Bundle\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Query\Expr\Base;
use ReSymf\Bundle\CmsBundle\Annotation\Table;
use ReSymf\Bundle\CmsBundle\Annotation\Form;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Class Page
 * @package ReSymf\Bundle\CmsBundle\Entity
 *
 * @ORM\Table()
 * @ORM\Entity
 *
 * @Table(sorting=true, paging=true, pageSize=10, filtering=true)
 * @Form(editLabel="Edytuj wpis", createLabel="Dodaj wpis", showLabel="Wpis")
 *
 * @author Piotr Francuz <piotr.francuz@bizneslan.pl>
 */
class Post
{
    /**
     * @var integer
     *
     * @Form(display=false)
     * @Table(display=false)
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var string
     *
     * @Table(hideOnDevice="tablet,phone", label="Nick")
     * @Form(fieldLabel="Nick",type="text",required=true)
     *
     * @ORM\Column(name="nick", type="string", length=255)
     */
    protected $nick;

    /**
     * @var string
     *
     * @Table(hideOnDevice="tablet,phone", label="E-mail")
     * @Form(fieldLabel="E-mail",type="text",required=true)
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    protected $email;

    /**
     * @var string
     *
     * @Table(format="html", length=300, label="Opis")
     * @Form(type="editor",required=true, fieldLabel = "Opis")
     *
     * @ORM\Column(name="content", type="text")
     */
    protected $content;

    /**
     * @Table(display=false)
     * @Form(fieldLabel="Files",type="file",required=true)
     *
     * @ORM\Column(type="array", length=255, nullable=true)
     */
    protected $path;

    /**
     * @ORM\Column(name="reg", type="boolean")
     *
     * @Table(format="text",hideOnDevice="tablet,phone", label="Akceptacja regulaminu")
     * @Form(type="bool",fieldLabel="Akceptacja regulaminu",required=true)
     */
    private $reg;

    /**
     * @ORM\Column(name="agree", type="boolean")
     *
     * @Table(format="text",hideOnDevice="tablet,phone", label="Zgoda")
     * @Form(fieldLabel="Zgoda",type="bool",required=true)
     */
    private $agree;

    /**
     * @return mixed
     */
    public function getAgree()
    {
        return $this->agree;
    }

    /**
     * @param mixed $agree
     */
    public function setAgree($agree)
    {
        $this->agree = $agree;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getReg()
    {
        return $this->reg;
    }

    /**
     * @param mixed $reg
     */
    public function setReg($reg)
    {
        $this->reg = $reg;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNick()
    {
        return $this->nick;
    }

    /**
     * @param string $title
     */
    public function setNick($title)
    {
        $this->nick = $title;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir() . '/' . $this->path;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/documents';
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir() . '/' . $this->path;
    }
}
