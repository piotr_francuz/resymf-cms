<?php

namespace ReSymf\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ReSymf\Bundle\CmsBundle\Annotation\Table;
use ReSymf\Bundle\CmsBundle\Annotation\Form;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Settings
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ReSymf\Bundle\CmsBundle\Entity\SettingsRepository")
 *
 * @Table(sorting=true, paging=true, pageSize=10, filtering=true)
 * @Form(editLabel="Edytuj ustawienia", createLabel="Dodaj ustawienia", showLabel="Ustawienia")
 *
 */
class Settings
{
    /**
     * @var integer
     *
     * @Form(display=false)
     * @Table(display=false)
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Table(hideOnDevice="tablet,phone", label="Nazwa aplikacji")
     * @Form(fieldLabel="Nazwa aplikacji",type="text",required=true)
     *
     * @ORM\Column(name="app_name", type="string", length=255)
     */
    private $appName;

    /**
     * @var string
     *
     * @Table(hideOnDevice="tablet,phone", label="Opis (SEO)")
     * @Form(fieldLabel="Opis (SEO)",type="text",required=true)
     *
     * @ORM\Column(name="seo_description", type="string", length=255)
     */
    private $seoDescription;

    /**
     * @var string
     *
     * @Table(hideOnDevice="tablet,phone", label="Słowa kluczowe (SEO)")
     * @Form(fieldLabel="Słowa kluczowe (SEO)",type="text",required=true)
     *
     * @ORM\Column(name="seo_keywords", type="string", length=255)
     */
    private $seoKeywords;

    /**
     * @var string
     *
     * @Table(hideOnDevice="tablet,phone", label="Separator (SEO)")
     * @Form(fieldLabel="Separator (SEO)",type="text",required=true)
     *
     * @ORM\Column(name="seo_separator", type="string", length=255)
     */
    private $seoSeparator;

    /**
     * @var string
     *
     * @Table(hideOnDevice="tablet,phone", label="Klucz GA")
     * @Form(fieldLabel="Klucz GA",type="text",required=true)
     *
     * @ORM\Column(name="ga_key", type="string", length=255)
     */
    private $gaKey;

    /**
     * @var Project
     *
     * @Form(type="relation", relationType="manyToOne", class="ReSymf\Bundle\CmsBundle\Entity\Theme", fieldLabel="Admin Theme")
     * @Table(display=false)
     *
     * @ORM\ManyToOne(targetEntity="Theme", inversedBy="settings")
     */
    private $theme;


    /**
     * @var Project
     *
     * @Form(type="relation", relationType="manyToOne", class="ReSymf\Bundle\CmsBundle\Entity\Page", fieldLabel="Strona Główna", displayField="title")
     * @Table(display=false)
     *
     * @ORM\ManyToOne(targetEntity="Page")
     */
    private $page;

    /**
     * @return \ReSymf\Bundle\ProjectManagerBundle\Entity\Project
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param \ReSymf\Bundle\ProjectManagerBundle\Entity\Project $project
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * @return mixed
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * @param mixed $theme
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get appName
     *
     * @return string
     */
    public function getAppName()
    {
        return $this->appName;
    }

    /**
     * Set appName
     *
     * @param string $appName
     * @return Settings
     */
    public function setAppName($appName)
    {
        $this->appName = $appName;

        return $this;
    }

    /**
     * Get seoDescription
     *
     * @return string
     */
    public function getSeoDescription()
    {
        return $this->seoDescription;
    }

    /**
     * Set seoDescription
     *
     * @param string $seoDescription
     * @return Settings
     */
    public function setSeoDescription($seoDescription)
    {
        $this->seoDescription = $seoDescription;

        return $this;
    }

    /**
     * Get seoKeywords
     *
     * @return string
     */
    public function getSeoKeywords()
    {
        return $this->seoKeywords;
    }

    /**
     * Set seoKeywords
     *
     * @param string $seoKeywords
     * @return Settings
     */
    public function setSeoKeywords($seoKeywords)
    {
        $this->seoKeywords = $seoKeywords;

        return $this;
    }

    /**
     * Get seoSeparator
     *
     * @return string
     */
    public function getSeoSeparator()
    {
        return $this->seoSeparator;
    }

    /**
     * Set seoSeparator
     *
     * @param string $seoSeparator
     * @return Settings
     */
    public function setSeoSeparator($seoSeparator)
    {
        $this->seoSeparator = $seoSeparator;

        return $this;
    }

    /**
     * Get gaKey
     *
     * @return string
     */
    public function getGaKey()
    {
        return $this->gaKey;
    }

    /**
     * Set gaKey
     *
     * @param string $gaKey
     * @return Settings
     */
    public function setGaKey($gaKey)
    {
        $this->gaKey = $gaKey;

        return $this;
    }
}
