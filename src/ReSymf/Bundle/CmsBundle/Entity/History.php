<?php
/**
 * Created by PhpStorm.
 * User: Piotr
 * Date: 2014-12-21
 * Time: 08:28
 */

namespace ReSymf\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ReSymf\Bundle\CmsBundle\Entity\User;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Class Page
 * @package ReSymf\Bundle\CmsBundle\Entity
 *
 * @ORM\Table()
 * @ORM\Entity
 *
 * @author Piotr Francuz <piotr.francuz@bizneslan.pl>
 */
class History {


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="User")
     */
    protected $author;

    /**
     * @var string
     *
     * @ORM\Column(name="saved_object_type", type="string", length=255)
     */
    private $savedObjectType;

    /**
     * @var string
     *
     * @ORM\Column(name="saved_object_id", type="string", length=255)
     */
    private $savedObjectId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="saving_date", type="datetime", nullable=true)
     */
    protected $savingDate;

    /**
     * @var array
     *
     * @ORM\Column(type="array", name="saved_object", nullable=true)
     */
    protected $savedObject;

    /**
     * @return string
     */
    public function getSavedObjectType()
    {
        return $this->savedObjectType;
    }

    /**
     * @param string $savedObjectType
     */
    public function setSavedObjectType($savedObjectType)
    {
        $this->savedObjectType = $savedObjectType;
    }

    /**
     * @return string
     */
    public function getSavedObjectId()
    {
        return $this->savedObjectId;
    }

    /**
     * @param string $savedObjectId
     */
    public function setSavedObjectId($savedObjectId)
    {
        $this->savedObjectId = $savedObjectId;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param int $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getSavingDate()
    {
        return $this->savingDate;
    }

    /**
     * @param mixed $savingDate
     */
    public function setSavingDate($savingDate)
    {
        $this->savingDate = $savingDate;
    }

    /**
     * @return mixed
     */
    public function getSavedObject()
    {
        return $this->savedObject;
    }

    /**
     * @param mixed $savedObject
     */
    public function setSavedObject($savedObject)
    {
        $this->savedObject = $savedObject;
    }
}