<?php
/**
 * Created by PhpStorm.
 * User: ppf
 * Date: 11/4/13
 * Time: 10:50 AM
 */

namespace ReSymf\Bundle\CmsBundle\Services;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Parser;


/**
 * Class AdminConfigurator
 * @package ReSymf\Bundle\CmsBundle\Services
 *
 * @author Piotr Francuz <piotr.francuz@bizneslan.pl>
 */
class AdminConfigurator
{

    private $adminConfig;
    private $siteConfig;
    private $resymfReader;
    private $em;
    private $kernel;
    private $security;

    public function __construct($em, $kernel, $resymfReader, $security)
    {
        $this->em = $em;
        $this->kernel = $kernel;
        $this->resymfReader = $resymfReader;
        $this->security = $security;

    }

    /**
     * set default values to adminmenu configuration
     *
     * @param $adminConfig
     * @return mixed
     * @throws \Symfony\Component\Config\Definition\Exception\Exception
     */
    public function setAdminDefaultValues($adminConfig)
    {
        foreach ($adminConfig as $key => $value) {

            if (!isset($adminConfig[$key]['role']) || empty($adminConfig[$key]['role'])) {
                $adminConfig[$key]['role'] = 'ROLE_ADMIN';
            }

            $user = $this->security->getToken()->getUser();
            if($user) {
                $roles = $user->getRoles();

                $foundRole = false;

                // check if user has role to access this menu position
                if (is_array($roles)) {
                    foreach ($roles as $role) {
                        if ($role->getRole() == $adminConfig[$key]['role']) {
                            $foundRole = true;
                            break;
                        }
                    }

                    if (!$foundRole) {
                        unset($adminConfig[$key]);
                        continue;
                    }
                }
            }

            if (!isset($adminConfig[$key]['type'])) {
                $adminConfig[$key]['type'] = 'crud';
            }

            if ($adminConfig[$key]['type'] == 'crud') {

                if (!isset($adminConfig[$key]['class'])) {
                    throw new Exception('No class set in ' . $key . ' area in admin.yml file');
                }

                if (!isset($adminConfig[$key]['remote'])) {
                    $adminConfig[$key]['remote'] = false;
                }

                if (!isset($adminConfig[$key]['object_prefix'])) {
                    $adminConfig[$key]['object_prefix'] = 'object';
                }

                $tableConfig = $this->resymfReader->readFormAnnotation($adminConfig[$key]['class']);
                $adminConfig[$key]['menuLabel'] = $tableConfig->menuLabel;
                $adminConfig[$key]['createLabel'] = $tableConfig->createLabel;
                $adminConfig[$key]['showLabel'] = $tableConfig->showLabel;
                $adminConfig[$key]['editLabel'] = $tableConfig->editLabel;
            }

            if ($adminConfig[$key]['type'] == 'custom_page' && !isset($adminConfig[$key]['template'])) {
                throw new Exception('No template set in ' . $key . ' area in adminmenu.yml file');
            }

            if (!isset($adminConfig[$key]['hidden'])) {
                $adminConfig[$key]['hidden'] = false;
            }
            if (!isset($adminConfig[$key]['icon'])) {
                $adminConfig[$key]['icon'] = 'file';
            }

            if (!isset($adminConfig[$key]['label'])) {
                $adminConfig[$key]['label'] = 'No Label';
            }

        }

        return $adminConfig;
    }

    public function readConfig()
    {

    }

    public function getAdminConfig()
    {
        $yaml = new Parser();
        try {
            $admin_file = $this->kernel->getRootDir() . '/config/admin.yml';
            $value = $yaml->parse(file_get_contents($admin_file));
        } catch (ParseException $e) {
            throw new ParseException("Unable to parse the YAML string: %s", $e->getMessage());
        }

        if (!$value['admin'] && !is_array($value['admin'])) {
            throw new ParseException('unable to find admin configuration');
        } else {
            $this->adminConfig = $this->setAdminDefaultValues($value['admin']);
        }

        return $this->adminConfig;
    }

    private function getConfigFromDb()
    {
        $objectType = 'ReSymf\Bundle\CmsBundle\Entity\Settings';
        $settingsObject = $this->em->getRepository($objectType)
            ->createQueryBuilder('q')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        if ($settingsObject) {
            $this->siteConfig['name'] = $settingsObject->getAppName();
            $this->siteConfig['description'] = $settingsObject->getSeoDescription();
            $this->siteConfig['keywords'] = $settingsObject->getSeoKeywords();
            $this->siteConfig['separator'] = $settingsObject->getSeoSeparator();
            $this->siteConfig['gakey'] = $settingsObject->getGaKey();
            if($settingsObject->getPage()) {
                $this->siteConfig['main_page'] = $settingsObject->getPage()->getId();
            }

            if ($settingsObject->getTheme() && $settingsObject->getTheme()->getName()) {
                $this->siteConfig['theme'] = $settingsObject->getTheme()->getName();
            } else {
                $this->siteConfig['theme'] = 'default';
            }
        }
    }

    public function getSiteConfig()
    {
        $yaml = new Parser();
        try {
            $admin_file = $this->kernel->getRootDir() . '/config/admin.yml';
            $value = $yaml->parse(file_get_contents($admin_file));
        } catch (ParseException $e) {
            throw new ParseException("Unable to parse the YAML string: %s", $e->getMessage());
        }

        if (!$value['site_config'] && !is_array($value['site_config'])) {
            throw new ParseException('unable to find site_config configuration');
        } else {
            $this->siteConfig = $value['site_config'];
        }
        $this->getConfigFromDb();

        return $this->siteConfig;
    }

    public function checkItemIfExistInMenu($itemName)
    {
        foreach ($this->getAdminConfig() as $key => $value) {
            if ($key == $itemName) {
                return $value;
            }
        }
        return false;
    }

}
