<?php
/**
 * Created by PhpStorm.
 * User: Piotr
 * Date: 2015-01-12
 * Time: 04:37
 */

namespace ReSymf\Bundle\CmsBundle\Services;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Parser;

class FrontendConfigurator {

	private $em;
	private $security;
	private $sortKey = 'order';
	private $kernel;

	function __construct( $em, $security, $kernel ) {
		$this->em       = $em;
		$this->security = $security;
		$this->kernel   = $kernel;
	}

	public function getFrontendConfig() {
		$yaml = new Parser();
		try {
			$admin_file = $this->kernel->getRootDir() . '/config/admin.yml';
			$value      = $yaml->parse( file_get_contents( $admin_file ) );
		} catch ( ParseException $e ) {
			throw new ParseException( "Unable to parse the YAML string: %s", $e->getMessage() );
		}

		if ( ! $value['frontend'] && ! is_array( $value['frontend'] ) ) {
			throw new ParseException( 'unable to find frontend configuration' );
		} else {
			$frontendConfig = $value['frontend'];
		}

		return $frontendConfig;
	}

	public function getConfigForView( $view ) {
		$config = $this->getFrontendConfig();

		$viewConfig = array();
		if ( isset( $config['views'] ) && is_array( $config['views'] ) ) {
			foreach ( $config['views'] as $element ) {
				if ( $element['templateName'] == $view ) {
					$viewConfig = $element;
				}
			}
		}

		return $viewConfig;
	}

	/**
	 * Get all object to display in view
	 *
	 * @param $view
	 * @param $pageNumber
	 *
	 * @return array
	 */
	public function getObjectsForView( $view, $pageNumber ) {

		$config = $this->getConfigForView( $view );

		$result  = array();
		$objects = array();
		$tableConfig = array();
		$count   = 0;
		$page_size = 1;

		if ( isset( $config ) && isset( $config['class'] ) ) {


			if ( $config['page_size'] && $config['page_size'] > 0 ) {

				$page_size = $config['page_size'];
				// set page number to 0 default, or decrease it by 1
				if ( $pageNumber > 0 ) {
					$pageNumber --;
				} else {
					$pageNumber = 0;
				}

				$firstObjectNumber = $pageNumber * $page_size;
				$objects           = $this->em
					->getRepository( $config['class'] )
					->createQueryBuilder( 'q' )
					->setMaxResults( $page_size )
					->setFirstResult( $firstObjectNumber )
					->getQuery()
					->getResult();
			} else {
				$objects = $this->em
					->getRepository( $config['class'] )
					->createQueryBuilder( 'q' )
					->getQuery()
					->getResult();
			}

			// auto sorting by sort_key set in admin.yml, from lowest to highest
			if ( isset( $config['sort_key'] ) ) {
				$this->sortKey = $config['sort_key'];
				usort( $objects, array( $this, 'cmp' ) );
			}

			$count = $this->countAllObjects( $config['class'] );

			$annotationReader = $this->get('resymfcms.annotation.reader');
			$tableConfig = $annotationReader->readTableAnnotation($config['class']);
		}


		$result['elements'] = $objects;
		$result['page']     = $pageNumber+1;
		$result['count']    = $count;
		$result['table_config'] = $tableConfig;
		$maxPage = ceil( $count / $page_size );
		$result['all_pages'] = range( 1, $maxPage );

		return $result;
	}

	/**
	 * Count all object in DB
	 *
	 * @param $class
	 *
	 * @return mixed
	 */
	public function countAllObjects( $class ) {

		$count = $this->em
			->getRepository( $class )
			->createQueryBuilder( 'q' )
			->select( 'count(q.id)' )
			->getQuery()
			->getSingleScalarResult();

		return $count;
	}

	/**
	 * get object by slug, search by field set in admin.yml (searchBy)
	 *
	 * @param $slug
	 *
	 * @return array|bool
	 * @throws ParseException
	 */
	public function getObjectForSlug( $slug ) {
		$config = $this->getFrontendConfig();

		if ( isset( $config['one_object_view'] ) && is_array( $config['one_object_view'] ) ) {
			foreach ( $config['one_object_view'] as $objectConfig ) {

				if ( $objectConfig['class'] && $objectConfig['searchBy'] ) {

					$object = $this->em->getRepository( $objectConfig['class'] )
					                   ->createQueryBuilder( 'q' )
					                   ->where( 'q.' . $objectConfig['searchBy'] . ' = :slug' )
					                   ->setParameter( 'slug', $slug )
					                   ->getQuery()
					                   ->getOneOrNullResult();
					if ( $object ) {
						return array( 'object' => $object, 'template' => $objectConfig['templateName'] );
					}
				}
			}
		}

		return false;
	}

	/**
	 * function used in usort, sort from lowest to highest
	 *
	 * @param $a
	 * @param $b
	 *
	 * @return int
	 */
	public function cmp( $a, $b ) {
		$sortKey = $this->sortKey;
		$getter  = 'get' . ucfirst( $sortKey );

		if ( method_exists( $a, $getter ) ) {
			if ( $a->$getter() == $b->$getter() ) {
				return 0;
			}

			return ( $a->$getter() < $b->$getter() ) ? - 1 : 1;
		} else {
			return 0;
		}
	}
}