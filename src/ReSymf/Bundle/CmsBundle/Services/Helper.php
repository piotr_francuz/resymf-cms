<?php

namespace ReSymf\Bundle\CmsBundle\Services;

class Helper {

    public function clean($var){
        $var = mysql_real_escape_string($var);
        $var = htmlspecialchars($var);
        return $var;
    }

    public function slugify($string)
    {
        $string = strtolower($string);
        $polskie = array(',', ' - ',' ','ę', 'Ę', 'ó', 'Ó', 'Ą', 'ą', 'Ś', 's', 'ł', 'Ł', 'ż', 'Ż', 'Ź', 'ź', 'ć', 'Ć', 'ń', 'Ń','-',"'","/","?", '"', ":", 'ś', '!','.', '&', '&amp;', '#', ';', '[',']','domena.pl', '(', ')', '`', '%', '”', '„', '…');
        $miedzyn = array('-','-','-','e', 'e', 'o', 'o', 'a', 'a', 's', 's', 'l', 'l', 'z', 'z', 'z', 'z', 'c', 'c', 'n', 'n','-',"","","","","",'s','','', '', '', '', '', '', '', '', '', '', '', '', '');
        $string = str_replace($polskie, $miedzyn, $string);

        $string = preg_replace('/[^0-9a-z\-]+/', '', $string);

        $string = preg_replace('/[\-]+/', '-', $string);

        $string = trim($string, '-');

        $string = stripslashes($string);

        $string = urlencode($string);

      return $string;
    }
}