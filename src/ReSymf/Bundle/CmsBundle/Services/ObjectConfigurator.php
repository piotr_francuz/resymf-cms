<?php
/**
 * Created by PhpStorm.
 * User: ppf
 * Date: 21.04.14
 * Time: 16:26
 */

namespace ReSymf\Bundle\CmsBundle\Services;


use Doctrine\ORM\EntityManager;
use ReSymf\Bundle\CmsBundle\Annotation\AnnotationReader;
use Symfony\Component\Security\Core\SecurityContext;

class ObjectConfigurator {

	private $adminConfigurator;
	private $reader;
	private $resymfReader;
	private $security;
	private $entityManager;
	private $kernel;
	private $helper;

	/**
	 * @param AdminConfigurator $adminConfigurator
	 * @param $annotationReader
	 * @param $resymfAnnotationReader
	 * @param SecurityContext $security
	 * @param EntityManager $entityManager
	 */
	public function __construct( AdminConfigurator $adminConfigurator, $annotationReader, $resymfAnnotationReader, SecurityContext $security, EntityManager $entityManager, $kernel, $helper ) {
		$this->adminConfigurator = $adminConfigurator;
		$this->reader            = $annotationReader; // set annotations reader
		$this->resymfReader      = $resymfAnnotationReader; // set annotations reader
		$this->security          = $security;
		$this->entityManager     = $entityManager;
		$this->kernel            = $kernel;
		$this->helper            = $helper;
	}

	public function  generateMultiSelectOptions( $classNameSpace, $object, $single = false ) {
		$multiSelect       = array();
		$displayMethodName = 'getName';

		if ( ! isset( $classNameSpace ) ) {
			return false;
		}
		$formConfig = new \stdClass;

		$reflectionClass = new \ReflectionClass( $classNameSpace );

		$properties         = $reflectionClass->getProperties();
		$formConfig->fields = array();
		foreach ( $properties as $reflectionProperty ) {
			$fields = array( 'q.id' );

			$annotation = $this->reader->getPropertyAnnotation( $reflectionProperty, 'ReSymf\Bundle\CmsBundle\Annotation\Form' );
			if ( null !== $annotation ) {
				if ( $annotation->getType() == 'relation' ) {

					$selectedIds = array();
					// get name of current field
					$fieldName = $reflectionProperty->getName();

					// get relation type
					$relationType = $annotation->getRelationType();

					$class        = $annotation->getClass();
					$displayField = $annotation->getDisplayField();
					if ( $displayField ) {
						$fields[]          = 'q.' . $displayField . ' as name';
						$displayMethodName = 'get' . $displayField;
					} else {
						$fields[] = 'q.name';
					}

					if ( $object ) {
						$methodName             = 'get' . $fieldName;
						$selectedOptionsObjects = $object->$methodName();
					} else {
						$selectedOptionsObjects = array();
					}

					if ( $relationType == 'oneToOne' || $relationType == 'manyToOne' ) {

						$selectedOptions = array();
						if ( $selectedOptionsObjects and ! is_array( $selectedOptionsObjects ) ) {

							$tempOption['name'] = $selectedOptionsObjects->$displayMethodName();
							$tempOption['id']   = $selectedOptionsObjects->getId();
							if ( $annotation->getTargetEntityField() ) {
								$tempOption['targetEntityField'] = $annotation->getTargetEntityField();
							}
							$selectedOptions[ $fieldName ] = $tempOption;

							$selectedIds[] = $selectedOptionsObjects->getId();

						} else {
							$selectedOptions = $selectedOptionsObjects;
						}
						$multiSelect[ $fieldName ]['selected'] = $selectedOptions;
					} else {

						if ( count( $selectedOptionsObjects ) > 0 ) {

							$selectedOptions = array();

							foreach ( $selectedOptionsObjects as $option ) {
								$tempOption         = array();
								$tempOption['name'] = $option->$displayMethodName();
								$tempOption['id']   = $option->getId();
								if ( $annotation->getTargetEntityField() ) {
									$tempOption['targetEntityField'] = $annotation->getTargetEntityField();
								}
								$selectedOptions[] = $tempOption;
							}

							$selectedIds = $this->array_value_recursive( 'id', $selectedOptions );

							// get all options to select
							$multiSelect[ $fieldName ]['selected'] = $selectedOptions;

						} else {
							$multiSelect[ $fieldName ]['selected'] = array();
						}
					}

					if ( $selectedIds ) {

						$allMultiSelectObjects = $this->entityManager
							->getRepository( $class )
							->createQueryBuilder( 'q' )
							->select( $fields )
							->where( 'q.id NOT IN (' . implode( ',', $selectedIds ) . ')' )
							->getQuery()
							->getResult();

					} else {
						$allMultiSelectObjects = $this->entityManager
							->getRepository( $class )
							->createQueryBuilder( 'q' )
							->select( $fields )
							->getQuery()
							->getResult();
					}


					$multiSelect[ $fieldName ]['all'] = $allMultiSelectObjects;

					if ( $relationType == 'oneToOne' || $relationType == 'manyToOne' ) {
						continue;
					}
//                    }

					// for toMany relations
					if ( $selectedOptionsObjects ) {
						$multiSelect[ $fieldName ]['entities'] = $selectedOptionsObjects;
					} else {
						$multiSelect[ $fieldName ]['entities'] = array();
					}
					$tableConfig                               = $this->resymfReader->readTableAnnotation( $class );
					$multiSelect[ $fieldName ]['table_config'] = $tableConfig;
					$multiSelect[ $fieldName ]['object_type']  = $this->getAdminConfigKeyByClassName( $class );

				} else if ( $annotation->getType() == 'template' ) {

					$fieldName = $reflectionProperty->getName();

					if ( $object ) {
						$methodName            = 'get' . $fieldName;
						$selectedOptionsObject = $object->$methodName();

					} else {
						$selectedOptionsObject = array();
					}

					$site_config = $this->adminConfigurator->getSiteConfig();
					$path        = $this->kernel->locateResource( '@' . $site_config['templates_bundle'] . '/Resources/views/cms/' );

					$templates = scandir( $path );
					foreach ( $templates as $template ) {
						$fileNameArray = explode( '.', $template );
						if ( $fileNameArray[1] ) {
							$fileName  = $fileNameArray[0];
							$tempArray = array( 'name' => $fileName, 'id' => $template );
							if ( $template == $selectedOptionsObject ) {
								$multiSelect[ $fieldName ]['selected'][] = $tempArray;
							} else {
								$multiSelect[ $fieldName ]['all'][] = $tempArray;
							}
						}
					}
				}
			}
		}

		return $multiSelect;
	}

	/**
	 * Get all values from specific key in a multidimensional array
	 *
	 * @param $key string
	 * @param $arr array
	 *
	 * @return null|string|array
	 */
	public function array_value_recursive( $key, array $arr ) {
		$val = array();
		array_walk_recursive( $arr, function ( $v, $k ) use ( $key, &$val ) {
			if ( $k == $key ) {
				array_push( $val, $v );
			}
		} );

		return $val;
	}

	private function getAdminConfigKeyByClassName( $className ) {
		$adminConfig = $this->adminConfigurator->getAdminConfig();

		foreach ( $adminConfig as $key => $value ) {

			if ( isset( $value['class'] ) && $value['class'] == $className ) {
				return $key;
			}
		}

		return false;
	}

	public function setAutoInputValuesFromAnnotations( $classNameSpace, $object ) {
		if ( ! isset( $classNameSpace ) ) {
			return false;
		}
		$formConfig = new \stdClass;

		$reflectionClass = new \ReflectionClass( $classNameSpace );

		$properties         = $reflectionClass->getProperties();
		$formConfig->fields = array();
		foreach ( $properties as $reflectionProperty ) {
			$annotation = $this->reader->getPropertyAnnotation( $reflectionProperty, 'ReSymf\Bundle\CmsBundle\Annotation\Form' );
			if ( null !== $annotation ) {
				if ( $annotation->getAutoInput() ) {
					$autoInputType = $annotation->getAutoInput();
					$newValue      = '';
					switch ( $autoInputType ) {
						case 'currentUserId':
							$user     = $this->security->getToken()->getUser();
							$newValue = $user;
							break;
						case 'currentDateTime' :
							$currentDate = new \DateTime( 'now' );
							$newValue    = $currentDate;
							break;
						default :
							$newValue = 'Wrong auto input type';
							break;
					}
					$fieldName  = $reflectionProperty->getName();
					$methodName = 'set' . $fieldName;
					$object->$methodName( $newValue );
				}
			}

		}

		return $object;
	}

	public function generateUniqueSlug( $adminConfigKey, $slug, $fieldName, $excludedId ) {

		$entities = $this->getEntitiesWithTheSameBaseSlug( $adminConfigKey );

		$notUnique = true;
		$count     = 2;
		foreach ( $entities as $entity ) {

			while ( $notUnique ) {
				$object = $this->getObjectFromSlug( $entity, $slug, $fieldName, $excludedId );

				if ( $object > 0 ) {
					$notUnique = true;
					$slug      = $slug . $count;
					$count ++;
				} else {
					$notUnique = false;
				}
			}
		}

		return $slug;
	}

	/**
	 * check if current entity has the same slug as other
	 *
	 * @param $adminConfigKey
	 *
	 * @return array
	 * @throws \Symfony\Component\Yaml\Exception\ParseException
	 */
	private function getEntitiesWithTheSameBaseSlug( $adminConfigKey ) {
		$entities    = array();
		$adminConfig = $this->adminConfigurator->getAdminConfig();

		$baseEntity = $adminConfig[ $adminConfigKey ];
		$baseSlug   = $baseEntity['slug'];

		foreach ( $adminConfig as $key => $value ) {
			if ( isset( $value['slug'] ) && $value['slug'] == $baseSlug ) {
				$entities[] = $adminConfig[ $key ]['class'];
			}
		}

		return $entities;
	}

	/**
	 * get object based by unique field - slug
	 *
	 * @param $className
	 * @param $slug
	 * @param $fieldName
	 *
	 * @return mixed
	 */
	public function getObjectFromSlug( $className, $slug, $fieldName, $excludedId ) {


		if(!$excludedId) {
			$excludedId = 0;
		}

		$pageObject = $this->entityManager
			->getRepository( $className )
			->createQueryBuilder( 'p' )
			->select( 'count(p.id)' )
			->where( 'p.' . $fieldName . ' = :slug' )
			->setParameter( 'slug', $slug )
			->andWhere( 'p.id != ' . $excludedId )
			->getQuery()
			->getSingleScalarResult();

		return $pageObject;
	}

	public function checkUniqueValuesFromAnnotations( $object, $adminConfigKey ) {
		$adminConfig = $this->adminConfigurator->getAdminConfig();

		$baseEntity     = $adminConfig[ $adminConfigKey ];
		$classNameSpace = $baseEntity['class'];

		if ( ! isset( $classNameSpace ) ) {
			return false;
		}
		$formConfig = new \stdClass;

		$reflectionClass    = new \ReflectionClass( $classNameSpace );
		$properties         = $reflectionClass->getProperties();
		$formConfig->fields = array();
		foreach ( $properties as $reflectionProperty ) {
			$annotation = $this->reader->getPropertyAnnotation( $reflectionProperty, 'ReSymf\Bundle\CmsBundle\Annotation\Form' );
			if ( null !== $annotation ) {
				if ( $annotation->getUnique() ) {
					$unique = $annotation->getUnique();
					switch ( $unique ) {
						case 'slug' :
							$fieldName     = $reflectionProperty->getName();
							$getMethodName = 'get' . $fieldName;
							$setMethodName = 'set' . $fieldName;

							$slug = $object->$getMethodName();

							// if no slug set name as slug
							if ( empty( $slug ) ) {
								if ( method_exists( $object, 'getTitle' ) ) {
									$slug = $object->getTitle();
								}
							}

							$excludedId = $object->getId();

							// check if generated or saved slug is unique
							$newValue = $this->generateUniqueSlug( $adminConfigKey, $this->helper->slugify( $slug ), $fieldName, $excludedId );
							$object->$setMethodName( $newValue );
							break;
						default :
							// do nothing
							break;
					}
				}
			}
		}

		return $object;
	}
}
