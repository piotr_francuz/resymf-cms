<?php
/**
 * Created by PhpStorm.
 * User: ppf
 * Date: 02.01.14
 * Time: 23:26
 */

namespace ReSymf\Bundle\CmsBundle\Services;

use ReSymf\Bundle\CmsBundle\Entity\History;

/**
 * Class ObjectHistory
 * @package ReSymf\Bundle\CmsBundle\Services
 *
 * @author Piotr Francuz <piotr.francuz@bizneslan.pl>
 */
class ObjectHistory
{
    private $em;
    private $security;

    function __construct($em, $security)
    {
        $this->em = $em;
        $this->security = $security;
    }

    public function getLastFromHistory($editObject, $type, $offset = 0)
    {
        $historyRepository = $this->em->getRepository('ReSymf\Bundle\CmsBundle\Entity\History')
            ->createQueryBuilder('q')
            ->where('q.savedObjectType = :sot')
            ->where('q.savedObjectId = :soi')
            ->setParameter('sot', $type)
            ->setParameter('soi', $editObject->getId())
            ->getQuery()
            ->getResult();

        if(is_array($historyRepository)) {
            $lastObject = count($historyRepository) -1;
            return $historyRepository[$lastObject];
        }

        return false;
    }

    public function clearHistory($editObject, $type)
    {
        $historyRepository = $this->em->getRepository('ReSymf\Bundle\CmsBundle\Entity\History')
            ->createQueryBuilder('q')
            ->where('q.savedObjectType = :sot')
            ->andWhere('q.savedObjectId = :soi')
            ->setParameter('sot', $type)
            ->setParameter('soi', $editObject->getId())
            ->getQuery()
            ->getResult();

        $this->em->remove($historyRepository);
    }

    public function  saveToHistory($editObject, $type)
    {
        $history = new History();
        $history->setSavingDate(new \DateTime('now'));
        $history->setSavedObject((array)$editObject);
        $history->setSavedObjectType($type);
        $history->setSavedObjectId($editObject->getId());

        $user = $this->security->getToken()->getUser();
        $history->setAuthor($user);

        $this->em->persist($history);
        $this->em->flush();

        $historyRepository = $this->em->getRepository('ReSymf\Bundle\CmsBundle\Entity\History')
            ->createQueryBuilder('q')
            ->where('q.savedObjectType = :sot')
            ->andWhere('q.savedObjectId = :soi')
            ->setParameter('sot', $type)
            ->setParameter('soi', $editObject->getId())
            ->getQuery()
            ->getResult();

        // add current version to all versions of this object
        $historyCount = $this->getHistoryCount($editObject, $type);

        if (count($historyRepository) > 10) {
            $this->em->remove($historyRepository[0]);
            $this->em->flush();
            $historyCount--;
        }

        return $historyCount;
    }

    public function getHistoryCount($editObject, $type)
    {
        $historyRepository = $this->em->getRepository('ReSymf\Bundle\CmsBundle\Entity\History')
            ->createQueryBuilder('q')
            ->where('q.savedObjectType = :sot')
            ->andWhere('q.savedObjectId = :soi')
            ->setParameter('sot', $type)
            ->setParameter('soi', $editObject->getId())
            ->getQuery()
            ->getResult();

        // add current version to all versions of this object
        $historyCount = count($historyRepository) + 1;

        return $historyCount;
    }
}