<?php
/**
 * Created by PhpStorm.
 * User: Piotr
 * Date: 2015-01-10
 * Time: 08:30
 */

namespace ReSymf\Bundle\CmsBundle\Services;

/**
 * Class ObjectSaver
 * class used to save object in admin panel, get values to fields from request
 *
 * @package ReSymf\Bundle\CmsBundle\Services
 */
class ObjectSaver {

    // entity manager
    private $em;
    private $encoderFactory;

    function __construct($em, $ef)
    {
        $this->em = $em;
        $this->encoderFactory = $ef;
    }

    /**
     * save fields in object, get values from request
     *
     * @param $formConfig
     * @param $editObject
     * @param $request
     * @param $type
     * @return mixed
     */
    public function saveObject($formConfig, $editObject, $request, $type) {

        /**
         * loop for all of fields in object [get fields from @form annotations]
         */
        foreach ($formConfig->fields as $field) {
            $fieldType = $field['type'];
            $fieldRelationType = $field['relationType'];
            $methodName = 'set' . $field['name'];
            $targetEntityField = $field['targetEntityField'];
            $autoInput = $field['autoInput'];

            if (!$autoInput) {
                switch ($fieldType) {
                    case 'relation':
                        $class = $field['class'];
                        $parameters = $request->get($field['name']);

                        $relationObjects = $this->em->getRepository($class)
                            ->createQueryBuilder('q')
                            ->where('q.id IN(:id)')
                            ->setParameter('id', $parameters)
                            ->getQuery()
                            ->getResult();

                        $addMethodName2 = 'set' . $field['name'];
                        if ($fieldRelationType == 'manyToOne' || $fieldRelationType == 'oneToOne') {
                            if ($relationObjects && $relationObjects[0]) {
                                $editObject->$addMethodName2($relationObjects[0]);
                            }
                        } else {
                            if ($relationObjects) {

                                $editObject->$addMethodName2($relationObjects);

                                foreach ($relationObjects as $relationObject) {

                                    if ($relationObject) {

                                        $addMethodName = 'set' . $type;
                                        $addMethodName2 = 'set' . $field['name'];

                                        if ($fieldRelationType == 'oneToMany') {
                                            $addMethodName2 = 'add' . $field['name'];

                                        }
                                        if ($fieldRelationType = 'manyToMany' || $fieldRelationType = 'multiselect') {
                                            $addMethodName2 = 'add' . $targetEntityField;
                                            $addMethodName = 'add' . $targetEntityField;
                                        } else { ///toOne
                                            $relationObject->$addMethodName($editObject);
                                        }

                                        if (method_exists($relationObject, $addMethodName2)) {
                                            $relationObject->$addMethodName2($editObject);
                                            $this->em->persist($relationObject);
                                            $this->em->flush();
                                        }
                                    }

                                }
                            }
                        }
                        break;
                    case 'date':
                        $editObject->$methodName(new \DateTime($request->get($field['name'])));
                        break;
                    case 'file':
                        $editObject->$methodName($request->get($field['name']));
                        break;
                    case 'template':
                        $template = $request->get($field['name']);
                        $addMethodName2 = 'set' . $field['name'];
                        $editObject->$addMethodName2($template[0]);
                        break;
                    case 'password':
                        $password = $request->get($field['name']);
                        if (!empty($password)) {
                            $addMethodName2 = 'set' . $field['name'];
                            $factory = $this->encoderFactory;
                            $encoder = $factory->getEncoder($editObject);
                            $pwd = $encoder->encodePassword($request->get($field['name']), $editObject->getSalt());
                            $editObject->$addMethodName2($pwd);
                        }
                        break;
                    case 'role':
                        $repository = $this->get('doctrine')->getRepository('ReSymfCmsBundle:Role');
                        $role = $repository->findOneBy(array('role' => $request->get($field['name'])));

                        if ($role) {
                            $role->addUser($editObject);
                            $editObject->addRole($role);
                            $this->em->persist($role);
                        }

                        break;
                    case 'bool':
                        if (!$request->get($field['name'])) {
                            $bool = 0;
                        } else {
                            $bool = 1;
                        }
                        $editObject->$methodName($bool);
                        break;
                    case 'number':
                        $editObject->$methodName(floatval($request->get($field['name'])));
                        break;
                    default:
                        $editObject->$methodName($request->get($field['name']));
                }
            }
        }
        return $editObject;
    }
}