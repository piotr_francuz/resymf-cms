<?php
namespace ReSymf\Bundle\CmsBundle\Command;


use ReSymf\Bundle\CmsBundle\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use ReSymf\Bundle\CmsBundle\Entity\User;
use ReSymf\Bundle\CmsBundle\Entity\Role;
use ReSymf\Bundle\CmsBundle\Entity\Theme;
use ReSymf\Bundle\CmsBundle\Entity\Settings;

class PopulateDatabaseCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('resymf:populate')
            ->setDescription('Populate database');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // get em
        $em = $this->getContainer()->get('doctrine')->getManager();

        // theme creation
        $theme = new Theme();
        $theme->setName('yeti');
        $em->persist($theme);
        $em->flush();


        // settings creation
        $settings = new Settings();
        $settings->setAppName('Aplikacja');
        $settings->setTheme($theme);
        $settings->setSeoSeparator('|');
        $settings->setSeoDescription('Awensome App');
        $settings->setSeoKeywords('symfony, bizneslan');
        $settings->setGaKey('sample key');
        $em->persist($settings);
        $em->flush();

        // user creation
        $user = new User();

        $email = 'pfrancuz1@gmail.com';
        $password = 'sample123';

        $user->setUsername('admin');
        $user->setEmail($email);
        $user->setIsActive(1);

        $factory = $this->getContainer()->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);
        $pwd = $encoder->encodePassword($password, $user->getSalt());
        $user->setPassword($pwd);

        $repository = $this->getContainer()->get('doctrine')->getRepository('ReSymfCmsBundle:Role');
        $role = $repository->findOneBy(array('role' => 'ROLE_ADMIN'));

        if (!$role) {
            $role = new Role();
            $role->setName('Administrator');
            $role->setRole('ROLE_ADMIN');
        }
        $role->addUser($user);

        $role2 = $repository->findOneBy(array('role' => 'ROLE_SUPER_ADMIN'));

        if (!$role2) {
            $role2 = new Role();
            $role2->setName('SuperAdmin');
            $role2->setRole('ROLE_SUPER_ADMIN');
        }
        $role2->addUser($user);

        $user->addRole($role2);
        $em->persist($user);
        $em->persist($role);
        $em->persist($role2);
        $em->flush();


        //front page creation
        $createDate = new \DateTime();
        $content = 'Welcome on front page';

        $page = new Page();
        $page->setAuthor($user);
        $page->setContent($content);
        $page->setCreateDate($createDate);
        $page->setSlug('');
        $page->setTemplate('index.html.twig');
        $page->setTitle('Main page');
        $em->persist($page);


        $em->flush();
        $output->writeln(sprintf('Created initial config'));
    }
}